package com.auradiateweb;

import static spark.Spark.port;
import static spark.Spark.get;
import static spark.Spark.staticFileLocation;

public class SparkMain {

	public static void main(String[] args) {
		
		port(4568);
		
		//Set static file location
		//For example get index.html insert url -> http://localhost:4568/index.html
		staticFileLocation("/public");
		
	
		WebPageController test = new WebPageController();
		get("/index",(request, response) -> test.getIndexPage(request, response));

		
	}

}
